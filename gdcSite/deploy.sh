#!/bin/bash

string=`sed '2q;d' log.txt`
procNum=`echo $string | cut -d "(" -f2 | cut -d ")" -f1`
kill $(echo $procNum)
/users/uwgdc/.local/bin/gunicorn gdcSite.wsgi 2> log.txt
