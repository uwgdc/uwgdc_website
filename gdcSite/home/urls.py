from django.conf.urls import url
from django.urls import path
from django.contrib import admin
from . import views

app_name="home"
urlpatterns = [
        url(r'contacts',views.contact,name='contact'),
        url(r'directions',views.directions,name='directions'),
		  url(r'projects',views.project,name='project'),
		  path(r'apply',views.apply,name='apply'),
		  url(r'showcase',views.showcase,name='showcase'),
		  url(r'ppHowTo',views.PPHowTo,name='pphowto'),
		  url(r'unitytutorialprep',views.UnityTutorial,name="unitytut"),
		  path(r'admin',admin.site.urls),
		  path(r'usefulLinks',views.usefulLinks,name="links"),
		  url(r'',views.index,name="index"),
]
