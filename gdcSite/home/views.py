from django.shortcuts import render, redirect
from django.template import Context, loader
from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.core.mail import send_mail

# Create your views here.
def index(request):
    template = loader.get_template("home/index.html")
    return HttpResponse(template.render())

def newindex(request):
    template = loader.get_template("home/newindex.html")
    return HttpResponse(template.render())

def contact(request):
    template = loader.get_template("home/contacts.html")
    return HttpResponse(template.render())

def directions(request):
    template = loader.get_template("home/directions.html")
    return HttpResponse(template.render())

def project(request):
	referer = request.META.get('HTTP_REFERER')
	if referer:
		if "apply" in referer:
			return render(request,"home/project.html",{'error_message':"Success",})
	return render(request,"home/project.html")

def apply(request):
	try:
		name = request.POST['Name']
		watiam = request.POST['Watiam']
		gitlabUserName = request.POST['GLUserName']
		if name == "" or watiam == "" or gitlabUserName=="":
			raise Exception("Name is not defined")
	except:
		return render(request,'home/project.html',{'error_message':"Form not filled out fully",})
	else:
		message = name + " would like to join the Pocket Pirates development team.<br>Watiam: " + watiam + "<br>Gitlab Username: " + gitlabUserName
		send_mail("Pocket Pirates New Application","","uw.gamedevclub.website@gmail.com",["uw.gamedevclub@gmail.com"],html_message=message)
		return redirect('home:project')

def showcase(request):
	return render(request,"home/showcase.html")

def PPHowTo(request):
	return render(request,"home/pphowto.html")

def UnityTutorial(request):
	return render(request,"home/unitytut.html")

def usefulLinks(request):
	return render(request,"home/usefullinks.html") 
