from django.contrib import admin
from .models import ClubMember, Skill
# Register your models here.

admin.site.register(ClubMember)
admin.site.register(Skill)
