from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Skill(models.Model):
    skill_Name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.skill_Name

class ClubMember(models.Model):
    name = models.CharField(max_length=50)
    skills = models.ManyToManyField(Skill)
    discord = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    
    def __str__(self):
        return  self.name


    
