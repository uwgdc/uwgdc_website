from django.conf.urls import url
from . import views

app_name="wiki"
urlpatterns= [
      url(r'^(?P<member_id>[0-9]+)/$',views.member, name="member"),  
		url(r'^$',views.wiki, name="index"),
        ]
