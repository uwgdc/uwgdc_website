from django.template import loader, Context
from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import ClubMember, Skill
# Create your views here.

def wiki(request):
	memberList = ClubMember.objects.order_by('name')
	skillList = Skill.objects.order_by('skill_Name')
	context = {'memberList': memberList,'skillList':skillList }
	template = loader.get_template("wiki/wiki.html")
	return render(request,'wiki/wiki.html',context)

def member(request,member_id):
	try:
		member = ClubMember.objects.get(pk=member_id)
	except ClubMember.DoesNotExist:
		raise Http404("Member does not exist")
	return render(request,'wiki/member.html',{'member':member})
